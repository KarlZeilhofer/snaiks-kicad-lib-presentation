EESchema Schematic File Version 2
LIBS:snaiks
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 6
Title "Other Symbols"
Date "2017-04-16"
Rev ""
Comp ""
Comment1 "www.zeilhofer.co.at/snaiks"
Comment2 "Snaiks Kicad Library Presentation"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L PinBool PIN?
U 1 1 58F34EC8
P 2050 1750
F 0 "PIN?" H 2050 1850 30  0000 C CNN
F 1 "PinBool" H 2050 1650 30  0000 C CNN
F 2 "" H 1850 1500 60  0000 C CNN
F 3 "" H 1850 1500 60  0000 C CNN
	1    2050 1750
	1    0    0    -1  
$EndComp
$Comp
L PinDouble PIN?
U 1 1 58F34F91
P 2450 1750
F 0 "PIN?" H 2450 1850 30  0000 C CNN
F 1 "PinDouble" H 2450 1650 30  0000 C CNN
F 2 "" H 2250 1500 60  0000 C CNN
F 3 "" H 2250 1500 60  0000 C CNN
	1    2450 1750
	1    0    0    -1  
$EndComp
Text Notes 1900 1450 0    60   ~ 0
Sheet Pins
Text Notes 1900 2200 0    60   ~ 0
Use this symbols for each signal entering\nand leaving a subsheet.
Wire Notes Line
	1800 1500 3950 1500
Wire Notes Line
	3950 1500 3950 2350
Wire Notes Line
	3950 2350 1800 2350
Wire Notes Line
	1800 2350 1800 1500
$EndSCHEMATC
