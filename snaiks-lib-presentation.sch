EESchema Schematic File Version 2
LIBS:snaiks
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 6
Title "Snaiks Kicad Library Presentation"
Date "2017-04-16"
Rev ""
Comp ""
Comment1 "www.zeilhofer.co.at/snaiks"
Comment2 "Snaiks Kicad Library Presentation"
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 7750 1450 2650 1250
U 581C7949
F0 "Objects with Analog Inputs" 100
F1 "analog.sch" 60
$EndSheet
$Sheet
S 7750 3050 2650 1250
U 581C794D
F0 "Objects with Digital Inputs" 100
F1 "digital.sch" 60
$EndSheet
$Sheet
S 7750 4650 2650 1250
U 58F345FF
F0 "Objects with Mixed Inputs" 100
F1 "mixed.sch" 60
$EndSheet
$Sheet
S 1350 1450 2650 1250
U 58F34A5C
F0 "Inputs and Outputs" 100
F1 "io.sch" 60
$EndSheet
$Sheet
S 1350 3200 2650 1200
U 58F34EBD
F0 "Other Symbols" 100
F1 "other.sch" 60
$EndSheet
$EndSCHEMATC
