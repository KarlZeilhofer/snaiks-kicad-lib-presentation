EESchema Schematic File Version 2
LIBS:snaiks
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 6
Title "Objects with Mixed Inputs"
Date "2017-04-16"
Rev ""
Comp ""
Comment1 "www.zeilhofer.co.at/snaiks"
Comment2 "Snaiks Kicad Library Presentation"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L HLD HLD?
U 1 1 58F347C8
P 1850 2500
F 0 "HLD?" H 1775 2897 60  0000 C CNN
F 1 "HLD" H 1775 2791 60  0000 C CNN
F 2 "" H 1650 2400 60  0000 C CNN
F 3 "" H 1650 2400 60  0000 C CNN
	1    1850 2500
	1    0    0    -1  
$EndComp
$Comp
L INT INT?
U 1 1 58F347CF
P 1800 3250
F 0 "INT?" H 1775 3562 60  0000 C CNN
F 1 "INT" H 1775 3456 60  0000 C CNN
F 2 "" H 1600 3150 60  0000 C CNN
F 3 "" H 1600 3150 60  0000 C CNN
	1    1800 3250
	1    0    0    -1  
$EndComp
$Comp
L DAMX DAMX?
U 1 1 58F347D6
P 1800 1700
F 0 "DAMX?" H 1775 2012 60  0000 C CNN
F 1 "DAMX" H 1775 1906 60  0000 C CNN
F 2 "" H 1600 1600 60  0000 C CNN
F 3 "" H 1600 1600 60  0000 C CNN
	1    1800 1700
	1    0    0    -1  
$EndComp
Text Notes 1300 1200 0    60   ~ 0
Analog Out
Text Notes 1250 2000 0    60   ~ 0
Dual Analog Multiplexer
Text Notes 1550 2750 0    60   ~ 0
Hold Gate
Text Notes 1550 3500 0    60   ~ 0
Integrator
Wire Notes Line
	1150 1250 1150 4700
Wire Notes Line
	1150 4700 2600 4700
Wire Notes Line
	2600 4700 2600 1250
Wire Notes Line
	2600 1250 1150 1250
$EndSCHEMATC
