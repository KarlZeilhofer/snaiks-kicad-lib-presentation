EESchema Schematic File Version 2
LIBS:snaiks
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 6
Title "Objects with Analog Inputs"
Date "2017-04-16"
Rev ""
Comp ""
Comment1 "www.zeilhofer.co.at/snaiks"
Comment2 "Snaiks Kicad Library Presentation"
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 7100 6800 0    60   ~ 0
Analog/Mixed Inputs
$Comp
L ABS ABS?
U 1 1 581C7AE6
P 1300 4550
AR Path="/581C7AE6" Ref="ABS?"  Part="1" 
AR Path="/581C7949/581C7AE6" Ref="ABS?"  Part="1" 
F 0 "ABS?" H 1325 4987 60  0000 C CNN
F 1 "ABS" H 1325 4881 60  0000 C CNN
F 2 "" H 1100 4450 60  0000 C CNN
F 3 "" H 1100 4450 60  0000 C CNN
	1    1300 4550
	1    0    0    -1  
$EndComp
$Comp
L AL AL?
U 1 1 581C7BE3
P 8500 3800
AR Path="/581C7BE3" Ref="AL?"  Part="1" 
AR Path="/581C7949/581C7BE3" Ref="AL?"  Part="1" 
F 0 "AL?" H 8500 4237 60  0000 C CNN
F 1 "AL" H 8500 4131 60  0000 C CNN
F 2 "" H 8300 3700 60  0000 C CNN
F 3 "" H 8300 3700 60  0000 C CNN
	1    8500 3800
	1    0    0    -1  
$EndComp
$Comp
L CMP CMP?
U 1 1 581C7C1B
P 8650 2000
AR Path="/581C7C1B" Ref="CMP?"  Part="1" 
AR Path="/581C7949/581C7C1B" Ref="CMP?"  Part="1" 
F 0 "CMP?" H 8600 2437 60  0000 C CNN
F 1 "CMP" H 8600 2331 60  0000 C CNN
F 2 "" H 8450 1900 60  0000 C CNN
F 3 "" H 8450 1900 60  0000 C CNN
	1    8650 2000
	1    0    0    -1  
$EndComp
$Comp
L DIF DIF?
U 1 1 581C7CC9
P 1400 2250
AR Path="/581C7CC9" Ref="DIF?"  Part="1" 
AR Path="/581C7949/581C7CC9" Ref="DIF?"  Part="1" 
F 0 "DIF?" H 1425 2562 60  0000 C CNN
F 1 "DIF" H 1425 2456 60  0000 C CNN
F 2 "" H 1300 2150 60  0000 C CNN
F 3 "" H 1300 2150 60  0000 C CNN
	1    1400 2250
	1    0    0    -1  
$EndComp
$Comp
L DIV DIV?
U 1 1 581C7CF3
P 2500 2250
AR Path="/581C7CF3" Ref="DIV?"  Part="1" 
AR Path="/581C7949/581C7CF3" Ref="DIV?"  Part="1" 
F 0 "DIV?" H 2525 2562 60  0000 C CNN
F 1 "DIV" H 2525 2456 60  0000 C CNN
F 2 "" H 2400 2150 60  0000 C CNN
F 3 "" H 2400 2150 60  0000 C CNN
	1    2500 2250
	1    0    0    -1  
$EndComp
$Comp
L HLD HLD?
U 1 1 581C7DE5
P 5600 2300
AR Path="/581C7DE5" Ref="HLD?"  Part="1" 
AR Path="/581C7949/581C7DE5" Ref="HLD?"  Part="1" 
F 0 "HLD?" H 5525 2697 60  0000 C CNN
F 1 "HLD" H 5525 2591 60  0000 C CNN
F 2 "" H 5400 2200 60  0000 C CNN
F 3 "" H 5400 2200 60  0000 C CNN
	1    5600 2300
	1    0    0    -1  
$EndComp
$Comp
L INT INT?
U 1 1 581C7E15
P 5550 3050
AR Path="/581C7E15" Ref="INT?"  Part="1" 
AR Path="/581C7949/581C7E15" Ref="INT?"  Part="1" 
F 0 "INT?" H 5525 3362 60  0000 C CNN
F 1 "INT" H 5525 3256 60  0000 C CNN
F 2 "" H 5350 2950 60  0000 C CNN
F 3 "" H 5350 2950 60  0000 C CNN
	1    5550 3050
	1    0    0    -1  
$EndComp
$Comp
L MAX MAX?
U 1 1 581C8023
P 2650 3700
F 0 "MAX?" H 2625 4012 60  0000 C CNN
F 1 "MAX" H 2625 3906 60  0000 C CNN
F 2 "" H 2450 3600 60  0000 C CNN
F 3 "" H 2450 3600 60  0000 C CNN
	1    2650 3700
	1    0    0    -1  
$EndComp
$Comp
L MIN MIN?
U 1 1 581C8065
P 1400 3700
F 0 "MIN?" H 1375 4012 60  0000 C CNN
F 1 "MIN" H 1375 3906 60  0000 C CNN
F 2 "" H 1200 3600 60  0000 C CNN
F 3 "" H 1200 3600 60  0000 C CNN
	1    1400 3700
	1    0    0    -1  
$EndComp
$Comp
L MUL MUL?
U 1 1 581C809B
P 2500 1500
F 0 "MUL?" H 2525 1812 60  0000 C CNN
F 1 "MUL" H 2525 1706 60  0000 C CNN
F 2 "" H 2400 1400 60  0000 C CNN
F 3 "" H 2400 1400 60  0000 C CNN
	1    2500 1500
	1    0    0    -1  
$EndComp
$Comp
L SC SC?
U 1 1 581C80DB
P 2200 4550
F 0 "SC?" H 2225 4987 60  0000 C CNN
F 1 "SC" H 2225 4881 60  0000 C CNN
F 2 "" H 2000 4450 60  0000 C CNN
F 3 "" H 2000 4450 60  0000 C CNN
	1    2200 4550
	1    0    0    -1  
$EndComp
$Comp
L SIGN SIGN?
U 1 1 581C8123
P 3200 4550
F 0 "SIGN?" H 3225 4987 60  0000 C CNN
F 1 "SIGN" H 3225 4881 60  0000 C CNN
F 2 "" H 3000 4450 60  0000 C CNN
F 3 "" H 3000 4450 60  0000 C CNN
	1    3200 4550
	1    0    0    -1  
$EndComp
$Comp
L ST ST?
U 1 1 581C8165
P 9850 2000
F 0 "ST?" H 9825 2649 60  0000 C CNN
F 1 "ST" H 9825 2543 60  0000 C CNN
F 2 "" H 9650 1900 60  0000 C CNN
F 3 "" H 9650 1900 60  0000 C CNN
F 4 "1" H 9825 2437 60  0000 C CNN "upperLimit"
F 5 "0" H 9825 2331 60  0000 C CNN "lowerLimit"
	1    9850 2000
	1    0    0    -1  
$EndComp
$Comp
L SUM SUM?
U 1 1 581C81C3
P 1400 1500
F 0 "SUM?" H 1425 1812 60  0000 C CNN
F 1 "SUM" H 1425 1706 60  0000 C CNN
F 2 "" H 1300 1400 60  0000 C CNN
F 3 "" H 1300 1400 60  0000 C CNN
	1    1400 1500
	1    0    0    -1  
$EndComp
$Comp
L DAMX DAMX?
U 1 1 581CC682
P 5550 1500
F 0 "DAMX?" H 5525 1812 60  0000 C CNN
F 1 "DAMX" H 5525 1706 60  0000 C CNN
F 2 "" H 5350 1400 60  0000 C CNN
F 3 "" H 5350 1400 60  0000 C CNN
	1    5550 1500
	1    0    0    -1  
$EndComp
Text Notes 950  950  0    60   ~ 0
Mathematical, Analog In/Analog Out
Text Notes 5050 1000 0    60   ~ 0
Mixed In/Analog Out
Text Notes 950  7200 0    60   ~ 0
Mixed In/Mixed Out
Text Notes 3800 7250 0    60   ~ 0
Analog In/Mixed Out
Text Notes 8400 1000 0    60   ~ 0
Analog In/Digital Out
Text Notes 8250 3850 0    60   ~ 0
Analog Line
Text Notes 1200 1750 0    60   ~ 0
Summation
Text Notes 2250 1750 0    60   ~ 0
Multiplication
Text Notes 1200 2500 0    60   ~ 0
Difference
Text Notes 2350 2500 0    60   ~ 0
Division
Text Notes 1150 3950 0    60   ~ 0
Minimum
Text Notes 2400 3950 0    60   ~ 0
Maximum
Text Notes 1150 4600 0    60   ~ 0
Absolute
Text Notes 1950 4600 0    60   ~ 0
Sign Changer
Text Notes 2900 4600 0    60   ~ 0
Signum Function
Text Notes 5000 1800 0    60   ~ 0
Dual Analog Multiplexer
Text Notes 5300 2550 0    60   ~ 0
Hold Gate
Text Notes 5300 3300 0    60   ~ 0
Integrator
Text Notes 8350 2350 0    60   ~ 0
Comparator
Text Notes 9500 2350 0    60   ~ 0
Schmitttrigger
Wire Notes Line
	850  4900 4200 4900
Wire Notes Line
	4200 4900 4200 1050
Wire Notes Line
	4200 1050 850  1050
Wire Notes Line
	4900 1050 4900 4900
Wire Notes Line
	4900 4900 6350 4900
Wire Notes Line
	6350 4900 6350 1050
Wire Notes Line
	6350 1050 4900 1050
Wire Notes Line
	7950 1050 7950 2700
Wire Notes Line
	7950 2700 10950 2700
Wire Notes Line
	10950 2700 10950 1050
Wire Notes Line
	10950 1050 7950 1050
$Comp
L AUD AUD?
U 1 1 58F34E45
P 9750 3650
F 0 "AUD?" H 9500 3800 60  0000 C CNN
F 1 "AUD" H 9450 3500 60  0000 C CNN
F 2 "" H 9250 3400 60  0000 C CNN
F 3 "" H 9250 3400 60  0000 C CNN
	1    9750 3650
	1    0    0    -1  
$EndComp
Text Notes 9900 3700 0    60   ~ 0
Analog Unit Delay\n(one sample time)
Wire Notes Line
	850  1050 850  4900
$Comp
L BESSEL4_ BESSEL4_?
U 1 1 58F37868
P 1450 5650
F 0 "BESSEL4_?" H 1420 5830 60  0000 C CNN
F 1 "BESSEL4_" H 1420 5460 60  0000 C CNN
F 2 "" H 1250 5550 60  0000 C CNN
F 3 "" H 1250 5550 60  0000 C CNN
F 4 "1" H 1280 5360 60  0000 C CNN "UpdateRateDivider"
	1    1450 5650
	1    0    0    -1  
$EndComp
Text Notes 1150 5250 0    60   ~ 0
Filters
Wire Notes Line
	850  5300 850  6700
Wire Notes Line
	850  6700 4000 6700
Wire Notes Line
	4000 6700 4000 5300
Wire Notes Line
	4000 5300 850  5300
Text Notes 2000 5750 0    60   ~ 0
4th order low pass Bessel filter\nwith corner frequency is \n1/8 of sample freqency
$Comp
L SAT SAT?
U 1 1 58F37E68
P 2450 3000
F 0 "SAT?" H 2350 3400 60  0000 C CNN
F 1 "SAT" H 2400 3300 60  0000 C CNN
F 2 "" H 2250 2900 60  0000 C CNN
F 3 "" H 2250 2900 60  0000 C CNN
F 4 "1.0" H 2440 3210 60  0000 C CNN "High"
F 5 "0.0" H 2570 3100 60  0000 C CNN "Low"
	1    2450 3000
	1    0    0    -1  
$EndComp
Text Notes 2300 3100 0    60   ~ 0
Saturation
$Comp
L SRL SRL?
U 1 1 58F3898C
P 1400 2900
F 0 "SRL?" H 1300 3200 60  0000 C CNN
F 1 "SRL" H 1300 2800 60  0000 C CNN
F 2 "" H 1200 2800 60  0000 C CNN
F 3 "" H 1200 2800 60  0000 C CNN
F 4 "1" H 1550 3200 60  0000 C CNN "Slewrate"
	1    1400 2900
	1    0    0    -1  
$EndComp
Text Notes 1050 3250 0    60   ~ 0
Slew Rate Limitation\nin units per second
Text Notes 8400 3150 0    60   ~ 0
Transmission
Wire Notes Line
	7950 3200 7950 4850
Wire Notes Line
	7950 4850 10950 4850
Wire Notes Line
	10950 4850 10950 3200
Wire Notes Line
	10950 3200 7950 3200
$EndSCHEMATC
