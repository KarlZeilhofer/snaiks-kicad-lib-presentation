EESchema Schematic File Version 2
LIBS:snaiks
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 6
Title "Objects with Digital Inputs"
Date "2017-04-16"
Rev ""
Comp ""
Comment1 "www.zeilhofer.co.at/snaiks"
Comment2 "Snaiks Kicad Library Presentation"
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 1000 1050 0    60   ~ 0
Digital In/Digital Out
$Comp
L AD AD?
U 1 1 581C83D5
P 1600 3950
F 0 "AD?" H 1575 4493 60  0000 C CNN
F 1 "AD" H 1575 4387 60  0000 C CNN
F 2 "" H 1400 3850 60  0000 C CNN
F 3 "" H 1400 3850 60  0000 C CNN
F 4 "1" H 1575 4281 60  0000 C CNN "Delay"
	1    1600 3950
	1    0    0    -1  
$EndComp
$Comp
L DAND DAND?
U 1 1 581C8432
P 8000 4550
F 0 "DAND?" H 7975 4987 60  0000 C CNN
F 1 "DAND" H 7975 4881 60  0000 C CNN
F 2 "" H 7800 4450 60  0000 C CNN
F 3 "" H 7800 4450 60  0000 C CNN
	1    8000 4550
	1    0    0    -1  
$EndComp
$Comp
L DDMX DDMX?
U 1 1 581C8524
P 9250 1900
F 0 "DDMX?" H 9225 2247 60  0000 C CNN
F 1 "DDMX" H 9225 2141 60  0000 C CNN
F 2 "" H 9050 1800 60  0000 C CNN
F 3 "" H 9050 1800 60  0000 C CNN
	1    9250 1900
	1    0    0    -1  
$EndComp
$Comp
L DL DL?
U 1 1 581C85B8
P 8100 1950
F 0 "DL?" H 8125 2347 60  0000 C CNN
F 1 "DL" H 8125 2241 60  0000 C CNN
F 2 "" H 7850 1850 60  0000 C CNN
F 3 "" H 7850 1850 60  0000 C CNN
	1    8100 1950
	1    0    0    -1  
$EndComp
$Comp
L DOR DOR?
U 1 1 581C8688
P 9500 4550
F 0 "DOR?" H 9475 4987 60  0000 C CNN
F 1 "DOR" H 9475 4881 60  0000 C CNN
F 2 "" H 9300 4450 60  0000 C CNN
F 3 "" H 9300 4450 60  0000 C CNN
	1    9500 4550
	1    0    0    -1  
$EndComp
$Comp
L FF FF?
U 1 1 581C86BE
P 1600 1850
F 0 "FF?" H 1575 2287 60  0000 C CNN
F 1 "FF" H 1575 2181 60  0000 C CNN
F 2 "" H 1400 1750 60  0000 C CNN
F 3 "" H 1400 1750 60  0000 C CNN
	1    1600 1850
	1    0    0    -1  
$EndComp
$Comp
L FFET FFET?
U 1 1 581C878A
P 2850 1850
F 0 "FFET?" H 2825 2287 60  0000 C CNN
F 1 "FFET" H 2825 2181 60  0000 C CNN
F 2 "" H 2650 1750 60  0000 C CNN
F 3 "" H 2650 1750 60  0000 C CNN
	1    2850 1850
	1    0    0    -1  
$EndComp
$Comp
L MF MF?
U 1 1 581C89C6
P 1600 2850
F 0 "MF?" H 1575 3393 60  0000 C CNN
F 1 "MF" H 1575 3287 60  0000 C CNN
F 2 "" H 1400 2750 60  0000 C CNN
F 3 "" H 1400 2750 60  0000 C CNN
F 4 "1" H 1575 3181 60  0000 C CNN "period"
	1    1600 2850
	1    0    0    -1  
$EndComp
$Comp
L MFET MFET?
U 1 1 581C8A05
P 2850 2850
F 0 "MFET?" H 2825 3393 60  0000 C CNN
F 1 "MFET" H 2825 3287 60  0000 C CNN
F 2 "" H 2650 2750 60  0000 C CNN
F 3 "" H 2650 2750 60  0000 C CNN
F 4 "1" H 2825 3181 60  0000 C CNN "period"
	1    2850 2850
	1    0    0    -1  
$EndComp
$Comp
L QOR QOR?
U 1 1 581C8B34
P 9500 5500
F 0 "QOR?" H 9475 5937 60  0000 C CNN
F 1 "QOR" H 9475 5831 60  0000 C CNN
F 2 "" H 9300 5400 60  0000 C CNN
F 3 "" H 9300 5400 60  0000 C CNN
	1    9500 5500
	1    0    0    -1  
$EndComp
$Comp
L TIMER TIMER?
U 1 1 581C8C8F
P 1500 6000
F 0 "TIMER?" H 1475 6312 60  0000 C CNN
F 1 "TIMER" H 1475 6206 60  0000 C CNN
F 2 "" H 1300 5900 60  0000 C CNN
F 3 "" H 1300 5900 60  0000 C CNN
	1    1500 6000
	1    0    0    -1  
$EndComp
Text Notes 1100 5400 0    60   ~ 0
Digital In/Analog Out
$Comp
L QAND QAND?
U 1 1 581DEC13
P 8000 5500
F 0 "QAND?" H 8025 5937 60  0000 C CNN
F 1 "QAND" H 8025 5831 60  0000 C CNN
F 2 "" H 7800 5400 60  0000 C CNN
F 3 "" H 7800 5400 60  0000 C CNN
	1    8000 5500
	1    0    0    -1  
$EndComp
Text Notes 7450 3800 0    60   ~ 0
Logic Combinations
Text Notes 1250 2200 0    60   ~ 0
RS-Flip-Flop
Text Notes 1300 3200 0    60   ~ 0
Mono-Flop
Text Notes 1200 4300 0    60   ~ 0
Activation Delay
Text Notes 7900 2250 0    60   ~ 0
Digital Line
Text Notes 8700 2250 0    60   ~ 0
Dual Digital Multiplexer
Text Notes 7650 4900 0    60   ~ 0
Dual AND Gate
Text Notes 7700 5850 0    60   ~ 0
Quad AND Gate
Text Notes 9150 4900 0    60   ~ 0
Dual OR Gate
Text Notes 9150 5850 0    60   ~ 0
Quad OR Gate
Text Notes 1150 6350 0    60   ~ 0
Timer\nOutput in seconds
Wire Notes Line
	900  1150 900  4550
Wire Notes Line
	900  4550 3550 4550
Wire Notes Line
	3550 4550 3550 1150
Wire Notes Line
	3550 1150 900  1150
Wire Notes Line
	7300 3900 7300 6050
Wire Notes Line
	7300 6050 10350 6050
Wire Notes Line
	10350 6050 10350 3900
Wire Notes Line
	10350 3900 7300 3900
Wire Notes Line
	7300 1100 7300 3150
Wire Notes Line
	7300 3150 10350 3150
Wire Notes Line
	10350 3150 10350 1100
Wire Notes Line
	10350 1100 7300 1100
Text Notes 7450 1000 0    60   ~ 0
Transmission
Wire Notes Line
	900  5500 900  7050
Wire Notes Line
	900  7050 3550 7050
Wire Notes Line
	3550 7050 3550 5500
Wire Notes Line
	3550 5500 900  5500
$Comp
L DUD DUD?
U 1 1 58F34D2B
P 8300 2700
F 0 "DUD?" H 8050 2850 60  0000 C CNN
F 1 "DUD" H 8000 2550 60  0000 C CNN
F 2 "" H 7800 2450 60  0000 C CNN
F 3 "" H 7800 2450 60  0000 C CNN
	1    8300 2700
	1    0    0    -1  
$EndComp
Text Notes 7900 3100 0    60   ~ 0
Digital Unit Delay\n(one sample time)
$EndSCHEMATC
